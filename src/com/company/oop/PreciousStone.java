package com.company.oop;

public class PreciousStone extends Stone {

    private String name;
    private double weight;
    private double price;

    public PreciousStone(String name, double weight, double price) {
        this.name = name;
        this.weight = weight;
        this.price = price;
    }

    public void necklace() {
        if (weight > 0.1 && price > 900.) {
            System.out.println(" !!! This plus to precious necklace: \n");
            print();
        }
        else {
            System.out.println("This is not plus to precious necklace: \n");
            print();
        }
    }


    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public void print() {
        System.out.println("Name: " + this.name + " " + "Weight: " + this.weight + " " +  "Price: " + this.price + "\n");
    }

}