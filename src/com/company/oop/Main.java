package com.company.oop;


public class Main {
    public static void main(String[] args) {

        double totalPreciousWeight = 0, totalPreciousPrice = 0;
        double totalSemiPreciousWeight = 0, totalSemiPreciousPrice = 0;

        for (int i = 2; i < 20; i += 2) {
            PreciousStone stone = new PreciousStone("Precious Stone №" + i * 12, i / 1000. + i / 100.,i * 15 + i * 500);
            totalPreciousWeight += stone.getWeight();
            totalPreciousPrice += stone.getPrice();
            stone.necklace();
        }
        for (int i = 1; i < 17; i += 2) {
            SemiPreciousStone stone2 = new SemiPreciousStone("SemiPrecious Stone №" + i * 15, i / 200 + i / 1000.,i * 15 + i * 100);
            totalSemiPreciousWeight += stone2.getWeight();
            totalSemiPreciousPrice += stone2.getPrice();
            stone2.necklace();
        }

        System.out.println("Total weight: " + totalPreciousWeight
                + "\n Total price: " + totalPreciousPrice + "\n");

        System.out.println("Total weight: " + totalSemiPreciousWeight
                + "\n Total price: " + totalSemiPreciousPrice);

    }
}
